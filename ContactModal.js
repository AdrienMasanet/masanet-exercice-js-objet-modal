class ContactModal extends Modal
{

    constructor ( title = "Contactez-nous", content, okButtonText = "Envoyer", closeable = true )
    {
        super( title, content, okButtonText, closeable );

        // CONTACT FORM DIV CONTAINER
        this.contactFormContainer = document.createElement( "form" );
        document.querySelector( "#modalContainer" + Modal.modalId ).appendChild( this.contactFormContainer );
        this.contactFormContainer.name = "contactFormContainer" + Modal.modalId;
        this.contactFormContainer.id = this.contactFormContainer.name;
        this.contactFormContainer.className = "contactFormContainer";
        this.modalOkButton.before( this.contactFormContainer );

        // CONTACT FORM DIV CONTAINER
        this.contactFormNameSurnameContainer = document.createElement( "div" );
        document.querySelector( "#contactFormContainer" + Modal.modalId ).appendChild( this.contactFormNameSurnameContainer );
        this.contactFormNameSurnameContainer.name = "contactFormNameSurnameContainer" + Modal.modalId;
        this.contactFormNameSurnameContainer.id = this.contactFormNameSurnameContainer.name;
        this.contactFormNameSurnameContainer.className = "contactFormNameSurnameContainer";

        // NAME FIELD IN THE CONTACT FORM
        this.contactFormName = document.createElement( "input" );
        document.querySelector( "#contactFormNameSurnameContainer" + Modal.modalId ).appendChild( this.contactFormName );
        this.contactFormName.type = "text";
        this.contactFormName.placeholder = "Rentrez votre prénom ici";
        this.contactFormName.name = "contactFormName" + Modal.modalId;
        this.contactFormName.id = this.contactFormName.name;
        this.contactFormName.className = "contactFormName";

        // NAME FIELD IN THE CONTACT FORM
        this.contactFormSurname = document.createElement( "input" );
        document.querySelector( "#contactFormNameSurnameContainer" + Modal.modalId ).appendChild( this.contactFormSurname );
        this.contactFormSurname.type = "text";
        this.contactFormSurname.placeholder = "Rentrez votre nom ici";
        this.contactFormSurname.name = "contactFormSurname" + Modal.modalId;
        this.contactFormSurname.id = this.contactFormSurname.name;
        this.contactFormSurname.className = "contactFormSurname";

        // MAIL FIELD IN THE CONTACT FORM
        this.contactFormMail = document.createElement( "input" );
        document.querySelector( "#contactFormContainer" + Modal.modalId ).appendChild( this.contactFormMail );
        this.contactFormMail.type = "mail";
        this.contactFormMail.placeholder = "Rentrez votre adresse mail ici";
        this.contactFormMail.name = "contactFormMail" + Modal.modalId;
        this.contactFormMail.id = this.contactFormMail.name;
        this.contactFormMail.className = "contactFormMail";

        // OBJECT FIELD IN THE CONTACT FORM
        this.contactFormMessageObject = document.createElement( "input" );
        document.querySelector( "#contactFormContainer" + Modal.modalId ).appendChild( this.contactFormMessageObject );
        this.contactFormMessageObject.type = "text";
        this.contactFormMessageObject.placeholder = "Rentrez votre objet ici";
        this.contactFormMessageObject.name = "contactFormMessageObject" + Modal.modalId;
        this.contactFormMessageObject.id = this.contactFormMessageObject.name;
        this.contactFormMessageObject.className = "contactFormMessageObject";

        // MESSAGE FIELD IN THE CONTACT FORM
        this.contactFormMessage = document.createElement( "textarea" );
        document.querySelector( "#contactFormContainer" + Modal.modalId ).appendChild( this.contactFormMessage );
        this.contactFormMessage.placeholder = "Rentrez votre message ici";
        this.contactFormMessage.name = "contactFormMessage" + Modal.modalId;
        this.contactFormMessage.id = this.contactFormMessage.name;
        this.contactFormMessage.className = "contactFormMessage";

        this.color = "whitesmoke";
        this.modalTitle.style.backgroundColor = this.color;
        this.modalContent.style.borderColor = this.color;
        this.modalOkButton.style.borderColor = this.color;
        this.contactFormContainer.style.borderColor = this.color;
        
        // BUTTON BEHAVIOUR
        this.modalOkButton.addEventListener( "click", () => this.contactFormContainer.submit() );
    }
}