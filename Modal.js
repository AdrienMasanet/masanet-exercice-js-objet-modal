class Modal
{

    static modalId = 0;

    constructor ( title, content, okButtonText, closeable = true )
    {
        this.title = title;
        this.content = content;
        this.okButtonText = okButtonText;
        Modal.modalId++;

        // MODAL BACKGROUND
        this.modalBackground = document.createElement( "div" );
        document.querySelector( "body" ).appendChild( this.modalBackground );
        this.modalBackground.name = "modalBackground" + Modal.modalId;
        this.modalBackground.id = this.modalBackground.name;
        this.modalBackground.className = "hidden " + "modalBackground";

        // MODAL CONTAINER
        this.modalContainer = document.createElement( "div" );
        document.querySelector( "#modalBackground" + Modal.modalId ).appendChild( this.modalContainer );
        this.modalContainer.name = "modalContainer" + Modal.modalId;
        this.modalContainer.id = this.modalContainer.name;
        this.modalContainer.className = "modalContainer";

        // MODAL EXIT CROSS
        if ( closeable ) {
            this.modalExitCross = document.createElement( "h3" );
            document.querySelector( "#modalContainer" + Modal.modalId ).appendChild( this.modalExitCross );
            this.modalExitCross.name = "modalExitCross" + Modal.modalId;
            this.modalExitCross.id = this.modalExitCross.name;
            this.modalExitCross.className = "modalExitCross";
            this.modalExitCross.innerHTML = "✗";
            this.modalExitCross.addEventListener( "click", () => this.hide() );
        }

        // MODAL TITLE
        this.modalTitle = document.createElement( "h2" );
        document.querySelector( "#modalContainer" + Modal.modalId ).appendChild( this.modalTitle );
        this.modalTitle.name = "modalTitle" + Modal.modalId;
        this.modalTitle.id = this.modalTitle.name;
        this.modalTitle.className = "modalTitle";
        this.modalTitle.innerHTML = this.title;

        // MODAL CONTENT
        this.modalContent = document.createElement( "h5" );
        document.querySelector( "#modalContainer" + Modal.modalId ).appendChild( this.modalContent );
        this.modalContent.name = "modalContent" + Modal.modalId;
        this.modalContent.id = this.modalContent.name;
        this.modalContent.className = "modalContent";
        this.modalContent.innerHTML = this.content;

        // OK BUTTON
        this.modalOkButton = document.createElement( "button" );
        document.querySelector( "#modalContainer" + Modal.modalId ).appendChild( this.modalOkButton );
        this.modalOkButton.name = "modalContent" + Modal.modalId;
        this.modalOkButton.id = this.modalOkButton.name;
        this.modalOkButton.className = "modalOkButton";
        this.modalOkButton.innerHTML = this.okButtonText;

    }

    show ()
    {
        this.modalBackground.classList.remove( "hidden" );
    }

    hide ()
    {
        this.modalBackground.classList.add( "hidden" );
    }
}