class ErrorModal extends Modal
{

    constructor ( title = "Erreur !", content, okButtonText = "Ok", closeable = false )
    {
        super( title, content, okButtonText, closeable );
        this.color = "red";
        this.modalTitle.style.backgroundColor = this.color;
        this.modalContent.style.borderColor = this.color;
        this.modalOkButton.style.borderColor = this.color;

        // BUTTON BEHAVIOUR
        this.modalOkButton.addEventListener( "click", () => this.hide() );
    }
}