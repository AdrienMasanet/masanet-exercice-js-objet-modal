class InfoModal extends Modal
{

    constructor ( title = "Information", content, okButtonText = "D'accord !", closeable = true )
    {
        super( title, content, okButtonText, closeable );
        this.color = "yellowgreen";
        this.modalTitle.style.backgroundColor = this.color;
        this.modalContent.style.borderColor = this.color;
        this.modalOkButton.style.borderColor = this.color;
        
        // BUTTON BEHAVIOUR
        this.modalOkButton.addEventListener( "click", () => this.hide() );
    }
}